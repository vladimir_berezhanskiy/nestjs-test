import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';
import { User } from '../../users/schemas/users.schema';
import { Max } from 'class-validator';
import { Skills } from '../../skills/schemas/skills.schema';

export type TasksDocument = Tasks & Document;
const startDate = new Date();

@Schema()
export class Tasks {
  @Prop({
    required: true,
  })
  name: string;

  @Prop({
    required: false,
    validate: [Max(255), 'Error! max string lengths'],
  })
  desc: string;

  @Prop({
    type: Date,
    required: false,
    default: startDate,
  })
  startDate: Date; // еще можно использовать для работы с датой и временем библиотеку Moment

  @Prop({
    required: false,
    type: Date,
    default: startDate.setDate(startDate.getDate() + 3),
  })
  endDate: Date;

  @Prop([{ type: mongoose.Schema.Types.ObjectId, ref: 'users' }])
  managers: User[];

  @Prop([{ type: mongoose.Schema.Types.ObjectId, ref: 'skills' }])
  requiredSkills: Skills[];
}

export const TasksSchema = SchemaFactory.createForClass(Tasks);
