import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type SkillsDocument = Skills & Document;

@Schema()
export class Skills {
  @Prop({
    required: true,
  })
  name: string;

  @Prop({
    required: false,
  })
  desc: string; /*аля владею js и что нибудь еще, можно сделать проверку на кол. символов как в примере с users*/
}

export const SkillsSchema = SchemaFactory.createForClass(Skills);
