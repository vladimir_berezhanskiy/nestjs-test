import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { UserSchema, User } from './schemas/users.schema';
import { userDto } from './dto/user.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User.name) private readonly userRepository: Model<User>,
  ) {}

  async findAll(): Promise<User[]> {
    return await this.userRepository.find().exec();
  }

  async create(userDto: userDto): Promise<User> {
    return await new this.userRepository({
      ...userDto,
      createdAt: new Date(),
    }).save();
  }
}
