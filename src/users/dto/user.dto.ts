import { IsEmail, IsEnum, IsNotEmpty, IsOptional, Min } from 'class-validator';

enum Roles {
  MANAGER = 'MANAGER', // здесь нужно перечислить все роли, но мне лень
}

export class userDto {
  @IsOptional()
  firstName: string;
  @IsOptional()
  lastName: string;
  @IsOptional()
  middleName: string;

  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @IsEnum(Roles, {
    message: 'Role invalid',
  })
  readonly roles: Roles;

  @IsNotEmpty()
  @Min(12)
  password: string;
}
