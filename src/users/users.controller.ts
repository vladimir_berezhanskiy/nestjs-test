import { Body, Controller, Get, Post } from '@nestjs/common';
import { UsersService } from './users.service';
import { userDto } from './dto/user.dto';

@Controller('users')
export class UsersController {
  constructor(private readonly userService: UsersService) {}
  @Get()
  async getAllUsers() {
    return this.userService.findAll();
  }

  @Post('add')
  async addUser(@Body() userDto: userDto) {
    return this.userService.create(userDto);
  }
}
