import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { isEmail, Min } from 'class-validator';
import { Project } from '../../projects/schemas/project.schema';
import * as mongoose from 'mongoose';
import { Skills } from '../../skills/schemas/skills.schema';

export type UserDocument = User & Document;

@Schema()
export class User {
  @Prop({
    required: true,
  })
  lastName: string;

  @Prop({
    required: true,
    validate: [Min(12), 'Пароль должен быть больше 12 символов'],
  })
  password: string;

  @Prop({
    required: true,
  })
  firstName: string;

  @Prop({
    required: false,
  })
  middleName: string;

  @Prop({
    required: false,
    validate: [isEmail, 'Неккоректный емаил'],
  })
  email: string;

  @Prop({
    required: true,
    enum: ['ADMIN', 'USER', 'MANAGER', 'DESIGNER', 'FRONTEND_DEVELOPER'],
  })
  roles: string;

  @Prop([{ type: mongoose.Schema.Types.ObjectId, ref: 'projects' }])
  projects: Project[];

  @Prop([{ type: mongoose.Schema.Types.ObjectId, ref: 'skills' }])
  skills: Skills[];
}

export const UserSchema = SchemaFactory.createForClass(User);
