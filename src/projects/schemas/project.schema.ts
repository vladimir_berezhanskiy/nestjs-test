import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';
import { User } from '../../users/schemas/users.schema';
import { Max } from 'class-validator';

export type ProjectDocument = Project & Document;

@Schema()
export class Project {
  @Prop({
    required: true,
  })
  name: string;

  @Prop({
    required: false,
    validate: [Max(255), 'Error max string lengths'],
  })
  desc: string;

  @Prop([String])
  customers: string[];

  @Prop([{ type: mongoose.Schema.Types.ObjectId, ref: 'users' }])
  managers: User[];
}

export const ProjectSchema = SchemaFactory.createForClass(Project);
